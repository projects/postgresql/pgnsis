!include "LogicLib.nsh"

!define StrStr "!insertmacro StrStr"

 
!macro StrStr ResultVar String SubString
  Push ${String}
  Push ${SubString}
  Call un.StrStr
  Pop ${ResultVar}
!macroend

Function un.StrStr
/*After this point:
  ------------------------------------------
  $R0 = SubString (input)
  $R1 = String (input)
  $R2 = SubStringLen (temp)
  $R3 = StrLen (temp)
  $R4 = StartCharPos (temp)
  $R5 = TempStr (temp)*/
 
  ;Get input from user
  Exch $R0
  Exch
  Exch $R1
  Push $R2
  Push $R3
  Push $R4
  Push $R5
 
  ;Get "String" and "SubString" length
  StrLen $R2 $R0
  StrLen $R3 $R1
  ;Start "StartCharPos" counter
  StrCpy $R4 0
 
  ;Loop until "SubString" is found or "String" reaches its end
  ${Do}
    ;Remove everything before and after the searched part ("TempStr")
    StrCpy $R5 $R1 $R2 $R4
 
    ;Compare "TempStr" with "SubString"
    ${IfThen} $R5 == $R0 ${|} ${ExitDo} ${|}
    ;If not "SubString", this could be "String"'s end
    ${IfThen} $R4 >= $R3 ${|} ${ExitDo} ${|}
    ;If not, continue the loop
    IntOp $R4 $R4 + 1
  ${Loop}
 
/*After this point:
  ------------------------------------------
  $R0 = ResultVar (output)*/
 
  ;Remove part before "SubString" on "String" (if there has one)
  StrCpy $R0 $R1 `` $R4
 
  ;Return output to user
  Pop $R5
  Pop $R4
  Pop $R3
  Pop $R2
  Pop $R1
  Exch $R0
FunctionEnd


Function un.Slice
 Exch $R0 ; input string
 Exch
 Exch $R1 ; to cut
 Push $R2
 Push $R3
 Push $R4
 Push $R5
 
 StrLen $R3 $R1
 StrCpy $R4 -1
 StrCpy $R5 0
 
 Loop:
 
  IntOp $R4 $R4 + 1
  StrCpy $R2 $R0 $R3 $R4
  StrCmp $R2 "" Done
  StrCmp $R2 $R1 0 Loop
 
   StrCpy $R5 1
 
   StrCmp $R4 0 0 +3
    StrCpy $R1 ""
    Goto +2
   StrCpy $R1 $R0 $R4
   StrLen $R2 $R0
   IntOp $R4 $R2 - $R4
   IntOp $R4 $R4 - $R3
   IntCmp $R4 0 0 0 +3
    StrCpy $R2 ""
    Goto +2
   StrCpy $R2 $R0 "" -$R4
   StrCpy $R0 $R1$R2
 
 Done:
 StrCpy $R1 $R5
 
 Pop $R5
 Pop $R4
 Pop $R3
 Pop $R2
 Exch $R1 ; slice? 0/1
 Exch
 Exch $R0 ; output string
FunctionEnd

Function isEmptyDir
  # Stack ->                    # Stack: <directory>
  Exch $0                       # Stack: $0
  Push $1                       # Stack: $1, $0
  FindFirst $0 $1 "$0\*.*"
  strcmp $1 "." 0 _notempty
    FindNext $0 $1
    strcmp $1 ".." 0 _notempty
      ClearErrors
      FindNext $0 $1
      IfErrors 0 _notempty
        FindClose $0
        Pop $1                  # Stack: $0
        StrCpy $0 1
        Exch $0                 # Stack: 1 (true)
        goto _end
     _notempty:
       FindClose $0
       ClearErrors
       Pop $1                   # Stack: $0
       StrCpy $0 0
       Exch $0                  # Stack: 0 (false)
  _end:
FunctionEnd

;Push "value to check"
;Push "comparisonlist"
Function Validate 
  Push $0
  Push $1
  Push $2
  Push $3 ;value length
  Push $4 ;count 1
  Push $5 ;tmp var 1
  Push $6 ;list length
  Push $7 ;count 2
  Push $8 ;tmp var 2
  Exch 9
  Pop $1 ;list
  Exch 9
  Pop $2 ;value
  StrCpy $0 1
  StrLen $3 $2
  StrLen $6 $1
  StrCpy $4 0
  lbl_loop:
    StrCpy $5 $2 1 $4
    StrCpy $7 0
    lbl_loop2:
      StrCpy $8 $1 1 $7
      StrCmp $5 $8 lbl_loop_next 0
      IntOp $7 $7 + 1
      IntCmp $7 $6 lbl_loop2 lbl_loop2 lbl_error
  lbl_loop_next:
  IntOp $4 $4 + 1
  IntCmp $4 $3 lbl_loop lbl_loop lbl_done
  lbl_error:
  StrCpy $0 0
  lbl_done:
  Pop $6
  Pop $5
  Pop $4
  Pop $3
  Pop $2
  Pop $1
  Exch 2
  Pop $7
  Pop $8
  Exch $0
FunctionEnd

