

!define NAMEPG 	"Postgre@Etersoft"
!define SMALNAMEPG 	"pgsql"


!define VERMAJOR "8"
!define VERMINOR "3"

!define VERPG "8.4.4"
!define sufix "eter"
!define REVPG "1"

!define SSLPATH "\var\ftp\pvt\Windows\4makepg\nsis\OpenSSL-Win32"
!define LIBMINGWPATHFROM "\usr\i586-pc-mingw32\sys-root\mingw\bin"
!define MSVCRPATHFROM "\var\ftp\pvt\Windows\4makepg\nsis\msvcr"
!define PGHEADPATHFROM ".."
!define PGADMINPATHFROM "\var\ftp\pvt\Windows\4makepg\nsis\pgAdmin III\1.10"
!define LICENSEPATH "AdditionalFiles\lic"
!define ICOPATH "AdditionalFiles\ico"
!define TIMEZONEFROM "\var\ftp\pvt\Windows\4makepg\nsis\timezone"



!define DEFAULTPORT	"5432"
!define DEFAULTENCODING	"UTF-8"
!define DEFAULTLOCALE	"Russia, Russia"


!define PRODUCTCODE 	"2804AADD-AD71-Ad7f-BE84-D10D36381DC9"


!define SERVICEDISPLAYNAME ${SMALNAMEPG}-${VERPG}-${sufix}${REVPG}
!define VALIDATEPASSWORDSTRING "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#$%^&*()_+|`\\=-}{$\":?><][';/.,"
